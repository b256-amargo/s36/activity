
// DEPENDENCIES SETUP
const express = require("express");
const mongoose = require("mongoose");

const taskRoute = require("./routes/taskRoutes.js");
// connection to the file containing the ROUTES

// SERVER SETUP
const app = express();

// SERVER PORT SETUP
const port = 4001;

// CLOUD SERVER CONNECTION
mongoose.connect("mongodb+srv://admin:admin1234@b256amargo.fojeanf.mongodb.net/B256_to-do?retryWrites=true&w=majority", {
// admin1234 and B256_to-do manually inserted
	useNewUrlParser: true,
	useunifiedTopology: true
});

// CHECKING THE CONNECTION
let db = mongoose.connection;
db.on("error", console.error.bind(console, "Connection error"));
db.once("open", () => console.log(`Connection to the cloud database successful`));

// MIDDLEWARES
app.use(express.json());
app.use(express.urlencoded({extended: true}));

app.use("/task", taskRoute);
// 1st argument is the parent route URI
// 2nd argument is the variable containing the connection to the ROUTES file

// SERVER LISTENING
app.listen(port, () => console.log(`Server is now currently running at port ${port}`));