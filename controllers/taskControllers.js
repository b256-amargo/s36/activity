
// This file contains all the functions and business logics of our application.

const Task = require("../models/Task.js");
// connection to the file containing the MODELS

module.exports.getAllTasks = () => {
	return Task.find({}).then(result => {
	// "Task" is from the model, which is where the database collection name is declared
	// .then() can have 2 variables, the 2nd one being dedicated for errors
		return result;
	});
};
// allows this file to be called as module on ROUTES

//-------------------------------------------------------------------------------------------------------------

// Controller function for creating a task
// The request body coming from the client was passed from the "taskRoute.js" file via the "req.body" as an argument and is renamed as a "requestBody" parameter in the controller file
module.exports.createTask = (requestBody) => {

	return Task.findOne({name: requestBody.name}).then((result, error) => {

		if(result !== null && result.name == requestBody.name) {

			return 'Duplicate Task Found';

		} else {

			// Creates a task object based on the Mongoose model "Task"
			let newTask = new Task({

				// Sets the "name" property with the value received from the client/Postman
				name: requestBody.name

			})

			// Saves the newly created "newTask" object in the MongoDB database
			// The "then" method waits until the task is stored in the database or an error is encountered before returning a "true" or "false" value back to the client/Postman
			// The "then" method will accept the following 2 arguments:
				// The first parameter will store the result returned by the Mongoose "save" method
				// The second parameter will store the "error" object
			// Compared to using a callback function on Mongoose methods discussed in the previous session, the first parameter stores the result and the error is stored in the second parameter which is the other way around
				// Ex.
					// newUser.save((saveErr, savedTask) => {})
			return newTask.save().then((savedTask, savedErr) => {

				// If an error is encountered returns a "false" boolean back to the client/Postman
				if(savedErr) {

					console.log(savedErr);
					// If an error is encountered, the "return" statement will prevent any other line or code below it and within the same code block from executing
					// Since the following return statement is nested within the "then" method chained to the "save" method, they do not prevent each other from executing code
					// The else statement will no longer be evaluated
					return 'Task Creation Failed';

				// Save successful, returns the new task object back to the client/Postman
				} else {

					return savedTask;

				} 
			})
		}
	})
}

// Controller function for deleting a task
// "taskId" is the URL parameter passed from the "taskRoute.js" file

// Business Logic
/*
	1. Look for the task with the corresponding id provided in the URL/route
	2. Delete the task using the Mongoose method "findByIdAndRemove" with the same id provided in the route
*/

// The task id retrieved from the "req.params.id" property coming from the client is renamed as a "taskId" parameter in the controller file
module.exports.deleteTask = (paramsId) => {

	// The "findByIdAndRemove" Mongoose method will look for a task with the same id provided from the URL and remove/delete the document from MongoDB
	// The Mongoose method "findByIdAndRemove" method looks for the document using the "_id" field
	return Task.findByIdAndRemove(paramsId).then((removeTask, err) => {

		// If an error is encountered returns a "false" boolean back to the client/Postman
		if(err) {

			console.log(err)
			return 'Task was not removed';

		// Delete successful, returns the removed task object back to the client/Postman
		} else {

			return 'Task successfully removed';

		}
	})
}

// Controller function for updating a task

// Business Logic
/*
	1. Get the task with the id using the Mongoose method "findById"
	2. Replace the task's name returned from the database with the "name" property from the request body
	3. Save the task
*/

// The task id retrieved from the "req.params.id" property coming from the client is renamed as a "taskId" parameter in the controller file
// The updates to be applied to the document retrieved from the "req.body" property coming from the client is renamed as "newContent"
module.exports.updateTask = (paramsId, requestBody) => {

	// The "findById" Mongoose method will look for a task with the same id provided from the URL
	// "findById" is the same as "find({"_id" : value})"
	// The "return" statement, returns the result of the Mongoose method "findById" back to the "taskRoute.js" file which invokes this function 
	return Task.findById(paramsId).then((result, err) => {

		// If an error is encountered returns a "false" boolean back to the client/Postman
		if(err) {

			console.log(error);
			return 'Error Found';

		} else {

			// Results of the "findById" method will be stored in the "result" parameter
			// It's "name" property will be reassigned the value of the "name" received from the request
			result.name = requestBody.name

			
			// Saves the updated object in the MongoDB database
			// The document already exists in the database and was stored in the "result" parameter
			// Because of Mongoose we have access to the "save" method to update the existing document with the changes we applied
			// The "return" statement returns the result of the "save" method to the "then" method chained to the "findById" method which invokes this function
			return result.save().then((updatedTask, err) => {

				// If an error is encountered returns a "false" boolean back to the client/Postman
				if(err) {
					
					// The "return" statement returns a "false" boolean to the "then" method chained to the "save" method which invokes this function
					console.log(err);
					return false;
				
				// Update successful, returns the updated task object back to the client/Postman
				} else {
					
					// The "return" statement returns a "false" boolean to the "then" method chained to the "save" method which invokes this function
					return updatedTask;
				}
			})
		}
	})
}

// ========================================================================================================

// ACTIVITY

module.exports.getTask = (request, response) => {
	let taskId = request.params.id;
	return Task.findOne({_id: taskId}).then((result, err) => {
		if(err) {
			console.log(error);
			return 'Error found';
		} 
		else {
			return result;
		};
	});
};

module.exports.updateTaskStatus = (taskId, requestBody) => {
	return Task.findById(taskId).then((result, err) => {
		if(err) {
			console.log(error);
			return 'Error found';
		} 
		else {
			result.status = "complete";
			return result.save().then((updatedTask, err) => {
				if(err) {
					console.log(err);
					return false;
				} 
				else {
					return updatedTask;
				};
			});
		};
	});
};
