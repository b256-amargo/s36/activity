
// This file contains all the schemas and models to be used by our application.

const mongoose = require("mongoose");

// SCHEMA CREATION
const taskSchema = new mongoose.Schema({
	name: String,
	// field: datatype
	status: {
		type: String,
		default: "pending"
	}
	// field: {options}
});

// MODEL CREATION
module.exports = mongoose.model("Task", taskSchema);
// allows this file to be called as module on CONTROLLERS
// 1st argument is the collection name
// 2nd argument is the schema to be used